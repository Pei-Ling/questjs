// Quest Implementation
// minimal implementation, currently without error handling; direct port of PsychToolbox Quest functions to JS

// All credit goes to Denis Pelli - see http://psych.nyu.edu/pelli/
// and the folks at PsychToolbox: - see http://psychtoolbox.org/
// see PsychToolbox documentation for much more detailed explanations of functions below
// suggestions, modifications, etc. are welcome

class questObj {
  constructor(tGuess, tGuessSd, pThreshold, beta, delta, gamma) {
    //Initialize values based on user input
    // Analogous to QuestCreate
    this.tGuess = tGuess
    this.tGuessSd = tGuessSd
    this.pThreshold = pThreshold
    this.beta = beta
    this.delta = delta
    this.gamma = gamma
    this.updatePdf = 1
    this.warnPdf = 1
    this.normalizePdf = 1
    this.grain = 0.01
    this.dim = 500
    this.i = []
    this.x = []
    this.pdf = []
    this.x2 = []
    this.p2 = []
    this.xThreshold = []
    this.s2 = []
    this.trialCount = []
    this.intensity = []
    this.response = []
    this.quantileOrder = []
  }

  recompute() {
    // bare minimum version of questRecompute 

    //prepare all the arrays: i,x,pdf,i2,x2,p2
    // i is centered array of size dim+1 from -dim/2 to dim/2 inclusive
    for (var idx = -this.dim / 2; idx < (this.dim / 2) + 1; ++idx) this.i.push(idx);
    // x  is i*grain -- an array of size dim+1 centered and incremented by grain
    this.x = this.i.map(x => x * this.grain);
    // pdf = probability density function e^(-0.5*(x/tGuessSd).^2), reaching 1 at 0 and scaled by user est. of tGuessSd
    this.pdf = this.x.map(x => Math.pow(Math.E, -0.5 * Math.pow((x / this.tGuessSd), 2)));
    // sum over all values of pdf, and normalize pdf
    let sumpdf = this.pdf.reduce((a, b) => a + b);
    this.pdf = this.pdf.map(x => x / sumpdf);

    // i2 is a centered array of size 2*dim + 1, from -dim to dim inclusive
    let i2 = []
    for (var idx = -this.dim; idx < this.dim + 1; ++idx) i2.push(idx)
    // x2 is i2*grain -- an array of size 2*dim+1 centered and incremented by grain
    this.x2 = i2.map(x => x * this.grain);
    // p2 is the Weibull function p2 = delta*gamma+(1-delta)*(1-(1-gamma)*exp(-10.^(beta*x2)))
    this.p2 = this.x2.map(x => this.delta * this.gamma + (1 - this.delta) * (1 - (1 - this.gamma) * Math.pow(Math.E, -Math.pow(10, (this.beta * x)))))

    // n stores the differences between each successive value of p2, equivalent to matlab's diff 
    let n = [];
    for (var i = 1; i < this.p2.length; i++) n.push(this.p2[i] - this.p2[i - 1])
    // indices (indexes) stores the indices of all values of n that are not zero 
    let indexes = [];
    for (var i = 0; i < n.length; i++)
      if (n[i] != 0) indexes.push(i);

    // subp2 and subx2 are subsets of p2 and x2 where n is nonzero (strictly monotonic)
    let subp2 = indexes.map(i => this.p2[i])
    let subx2 = indexes.map(i => this.x2[i])
    // Use linear interpolation to find the value of x2 that corresponds to pThreshold  
    for (var i = 1; i < subp2.length; i++) {
      let lb = subp2[i - 1]
      let ub = subp2[i]
      if ((this.pThreshold - lb >= 0) && (this.pThreshold - ub <= 0)) {
        let pinterp_1 = lb;
        let pinterp_2 = ub;
        let xinterp_1 = subx2[i - 1];
        let xinterp_2 = subx2[i];

        this.xThreshold = xinterp_1 + ((this.pThreshold - pinterp_1) / (pinterp_2 - pinterp_1)) * (xinterp_2 - xinterp_1)
        break
      }
    }

    // P2 is now the Weibull function delta*gamma+(1-delta)*(1-(1-gamma)*exp(-10.^(beta*(x+xThreshold)))); x represents log10 contrast relative to threshold; now centered on threshold (x = 0, y = pThreshold)
    this.p2 = this.x2.map(x => this.delta * this.gamma + (1 - this.delta) * (1 - (1 - this.gamma) * Math.pow(Math.E, -Math.pow(10, (this.beta * (x + this.xThreshold))))))

    // s2 corresponds to a 2*length(p2) matrix; row 1 is (1-p2) and row 2 is p2, and both are mirrored
    let invp2 = this.p2.map(x => 1 - x);
    let revp2 = this.p2.slice().reverse();
    this.s2 = [invp2.reverse(), revp2]

    // initialize trial count, as well as intensity and response arrays
    if (this.intensity.length == 0 || this.response.length == 0) {
      this.trialCount = 0;
      this.intensity = Array(10000).fill(0);
      this.response = Array(10000).fill(0);
    }

    // Get optimal quantile order; this quantileOrder yields a quantile  that is the most
    // informative intensity for the next trial
    let pL = this.p2[0]
    let pH = this.p2[this.p2.length - 1]
    let eps = Number.EPSILON
    let pE = pH * Math.log(pH + eps) - pL * Math.log(pL + eps) + (1 - pH + eps) * Math.log(1 - pH + eps) - (1 - pL + eps) * Math.log(1 - pL + eps)
    pE = 1 / (1 + Math.pow(Math.E, pE / (pL - pH)));
    this.quantileOrder = (pE - pL) / (pH - pL);
    
    // Skip some recomputing here that's not strictly necessary (taken care of by update), but can be added back in once the meat of this is done
    if (this.normalizePdf == 1) {
      let sumpdf = this.pdf.reduce((a, b) => a + b);
      this.pdf = this.pdf.map(x => x / sumpdf); //we already do this above, but just for consistency with PsychToolbox code
    }
  }

  update(intensity, response) {
    // minimal version of questUpdate 

    // Force intensity to finite bounds
    let inten = Math.max(-1e+10, Math.min(1e+10, intensity));
    // ii = size of pdf + this.i - round(inten-this.tGuess)/grain (will be used as indices for s2)
    let ii = this.i.map(i => this.pdf.length + i - Math.round((inten - this.tGuess) / this.grain));
    // adjust ii if initial value is less than 1, or final value is greater than p2.length
    if (ii[0] < 1 || ii[ii.length - 1] > this.p2.length) {
      if (ii[0] < 1) {
        ii = ii.map(i => i + 1 - ii[0]);
      } else {
        ii = ii.map(i => i + q.p2.length - ii[ii.length - 1])
      }
    }

    // update pdf based on response    
    for (var i = 0; i < this.pdf.length; i++) {
      this.pdf[i] = this.pdf[i] * this.s2[response][ii[i]]
    }

    // normalize pdf
    if (this.normalizePdf == 1) {
      let sumpdf = this.pdf.reduce((a, b) => a + b);
      this.pdf = this.pdf.map(x => x / sumpdf);
    }

    // record of trials
    this.intensity[this.trialCount] = intensity
    this.response[this.trialCount] = response

		// update trial count
    this.trialCount += 1
  }

  mean() {
		// Use QuestMean to estimate threshold
    
    // First, some elementwise multiplication of pdf by intensity
    let pdf_times_x = []
    for (var i = 0; i < this.pdf.length; i++) {
      pdf_times_x.push(this.pdf[i] * this.x[i])
    }
		// then, add mean of pdf to tGuess to get estimate of threshold
    let t = this.tGuess + pdf_times_x.reduce((a, b) => a + b) / this.pdf.reduce((a, b) => a + b)
  
    return t
  }

  quantile(qt = 0) {
  	// Use QuestQuantile to get quantile of pdf; if qt is not specified, it will be taken from the 'quantileOrder' property of the quest object (default)

    if (qt == 0){qt = this.quantileOrder}
    
		// p = cumulative sum of pdf
    let p = [];
    this.pdf.reduce(function(a, b, i) {
      return p[i] = a + b;
    }, 0);
		
    // add -1 to beginning of p; get difference between points & find nonzero indices (monotonic); QuestQuantile line 67
    p.reverse()
    p.push(-1)
    p.reverse()
    let index = []
    for (var i = 1; i < p.length; i++) {
      let diffp = p[i] - p[i - 1]
      if (diffp > 0) {
        index.push(i)
      }
    }
    p.shift() // remove -1 from beginning of pdf 

		// Linear Interpolation to get value of x corresponding to desired quantileOrder
    let delta_t = 0;
    let subp = index.map(i => p[i])
    let subx = index.map(i => this.x[i])
    let v = qt * p[p.length - 1]
    for (var i = 1; i < subp.length; i++) {
      let lb = subp[i - 1]
      let ub = subp[i]
      if ((v - lb >= 0) && (v - ub <= 0)) {
        let pinterp_1 = lb;
        let pinterp_2 = ub;
        let xinterp_1 = subx[i - 1];
        let xinterp_2 = subx[i];

        delta_t = xinterp_1 + ((v - pinterp_1) / (pinterp_2 - pinterp_1)) * (xinterp_2 - xinterp_1);
        break
      }
    }
    
    // add delta_t to initial tGuess
    let t = this.tGuess + delta_t;

		// handle extreme quantileOrder
    if (qt < p[0]) {
      t = this.tGuess + this.x[0]
    }
    if (qt > p[p.length - 1]) {
      t = this.tGuess + this.x[this.x.length - 1]
    }
		
    // return intensity value at desired quantile
    return t
  }
  
  sd(){
  	// QuestSD
    // Gets SD of threshold distribution
  	let sumpdf = this.pdf.reduce((a, b) => a + b);
    let pdf_times_x2 = []
    let pdf_times_x = []
    for (var i = 0; i < this.pdf.length; i++) {
      pdf_times_x2.push(this.pdf[i] * Math.pow(this.x[i],2))
      pdf_times_x.push(this.pdf[i]*this.x[i])
    }
    
    let sum_pdf_times_x2 = pdf_times_x2.reduce((a, b) => a + b);
    let sum_pdf_times_x = pdf_times_x.reduce((a, b) => a + b);
    
    let sd = Math.sqrt(sum_pdf_times_x2/sumpdf - Math.pow(sum_pdf_times_x/sumpdf,2))
    
    console.log(sd)
    return sd
  
  }
}

